# Módulo de Control #

Este proyecto contempla la implementación de un módulo de control de parámetros de un sistema. Se desarrolla sobre la placa de10nano.

La comunicación se realiza por JTAG UART y se ocupa el procesador NIOS II.

En una primera etapa se construye un pequeño periférico **Control PIO** que maneja registros dentro del procesador NIOS II para comunicación entre entrada y salida de datos.

En una segunda etapa se construye un módulo en Verilog para el manejo de señales provenientes del **NIOS II** y comunicación con la **FPGA**. La idea es que por medio del **status register** se tenga información sobre la escritura/lectura correcta de datos en la transacción.


