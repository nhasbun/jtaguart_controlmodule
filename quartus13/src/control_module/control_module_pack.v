`include "control_module.v"

module control_module_pack #(
  parameter BITDEPTH   = 32,
  parameter BYTEDEPTH  = 8,
  parameter ADDR_WIDTH = 2,
  parameter NUM_PARAM  = 2**ADDR_WIDTH
  )(
  input clock_50, reset_n,

  output [BITDEPTH - 1 : 0]  parameter_id,
  output [BYTEDEPTH - 1 : 0] parameter_value,
  output new_data
);

wire [31:0] control_module_address;
wire [31:0] control_module_value;
wire [7:0]  control_module_pending;
wire [7:0]  control_module_rw_signal;

niosii nios (
  .clock_50_clk                                     (clock_50),
  .reset_n_reset_n                                  (reset_n),

  .control_pio_address_external_connection_export (control_module_address),
  .control_pio_value_external_connection_export   (control_module_value),
  .control_pio_pending_external_connection_export (control_module_pending),
  .control_pio_rw_external_connection_export      (control_module_rw_signal)
);

control_module #(
      .BITDEPTH(BITDEPTH),
      .BYTEDEPTH(BYTEDEPTH),
      .ADDR_WIDTH(ADDR_WIDTH),
      .NUM_PARAM(NUM_PARAM)
)control_module (
  .clock_50     (clock_50),
  .reset_n      (reset_n),

  .nios_address (control_module_address),
  .nios_value   (control_module_value),
  .readwrite    (control_module_rw_signal),
  .pending      (control_module_pending),

  .parameter_id     (parameter_id),
  .parameter_value  (parameter_value),
  .new_data         (new_data)
);

endmodule