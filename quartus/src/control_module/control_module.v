`include "true_dual_port_ram.v"

module control_module #(
  parameter BITDEPTH   = 32,
  parameter BYTEDEPTH  = 8,
  parameter ADDR_WIDTH = 2,
  parameter NUM_PARAM  = 2**ADDR_WIDTH
  )(
  // General
  input clock_50, reset_n,

  // NIOS II side
  input [BITDEPTH - 1 : 0] nios_address,
  inout [BITDEPTH - 1 : 0] nios_value,
  input [BYTEDEPTH - 1 : 0] readwrite,
  output [BYTEDEPTH - 1: 0] pending,

  // FPGA side
  output reg [BITDEPTH - 1 : 0]  parameter_id,
  output reg [BYTEDEPTH - 1 : 0] parameter_value,
  output reg new_data
  );

parameter GROUND = 1'b0;
parameter HIGHZ = {(BITDEPTH){1'bz}};

// Definimos estados internos
reg [1:0] state;
parameter IDLE = 2'd0, FASE1 = 2'd1, FASE2 = 2'd2;
initial state = IDLE;

// Cambios de estado
wire rw; // senal de lectura o escritura
assign rw = (readwrite > 1'd0)? 1 : 0;

wire ready; // senal de trabajos de FASE1 y FASE2 listos (para read y write).

always @(posedge clock_50 or negedge reset_n) begin
  if(reset_n == 0) state <= IDLE;
  else begin
    case(state)
      IDLE: begin
        if(rw == 1) state <= FASE1;
        if(rw == 0) state <= IDLE;
      end
      FASE1: begin
        if(pending == 1) state <= FASE2;
        if(pending == 0) state <= FASE1;
      end
      FASE2: begin
        if(ready == 1) state <= IDLE;
        if(ready == 0) state <= FASE2;
      end
    endcase
  end
end

// Creacion de bit pending segun estado
reg pending_bit; initial pending_bit = 0;
assign pending[BYTEDEPTH - 1 : 1] = {(BYTEDEPTH - 1){1'b0}};
assign pending[0] = pending_bit;

reg reading; initial reading = 0;
reg writing; initial writing = 0;

always @(state) begin
  case(state)
    IDLE: begin
      pending_bit = 0;
      reading = 0;
      writing = 0;
    end
    FASE1: begin
      pending_bit = 1;
      reading = readwrite[0];
      writing = readwrite[1];
    end
    FASE2: begin
      pending_bit = 1;
    end
  endcase
end


// Maquina de estado para reading
reg [1:0] read_state;
parameter READ_IDLE = 0, READ_FASE1 = 1, READ_FASE2 = 2, READ_FASE3 = 3;
initial read_state = READ_IDLE;

always @(posedge clock_50 or negedge reset_n) begin
  if(reset_n == 0) read_state <= READ_IDLE;
  else begin
    case(read_state)
      READ_IDLE: begin
        if(reading == 0) read_state <= READ_IDLE;
        else            read_state <= READ_FASE1;
      end
      READ_FASE1: begin
        if(reading == 0) read_state <= READ_IDLE;
        else             read_state <= READ_FASE2;
      end
      READ_FASE2: begin
        if(reading) read_state <= READ_FASE3;
        else        read_state <= READ_IDLE;
      end
      READ_FASE3: begin
        if(reading) read_state <= READ_FASE3;
        else        read_state <= READ_IDLE;
      end
    endcase
  end
end

// Acciones para reading
reg [BITDEPTH - 1 : 0] read_add; initial read_add = 0;
reg [BITDEPTH - 1 : 0] dataread; initial dataread = 0;
reg read_ready; initial read_ready = 0;

always @(read_state) begin
  case(read_state)
    READ_IDLE:  read_ready = 0;
    READ_FASE1: read_add = nios_address;
    READ_FASE2: dataread = ram_q_a;
    READ_FASE3: read_ready = 1;
  endcase
end

// Maquina de estado para writing
reg [2:0] write_state;
parameter WRITE_IDLE = 0, W_FASE1 = 1, W_FASE2 = 2, W_FASE3 = 3, W_FASE4 = 4,
          W_FASE5 = 5;
initial write_state = WRITE_IDLE;

reg write_ready; initial write_ready = 0;

always @(posedge clock_50 or negedge reset_n) begin
  if(reset_n == 0) write_state <= WRITE_IDLE;
  else begin
    case(write_state)
      WRITE_IDLE:
      // En espera de senal de write, cuando se detecta, pasa a fase 1
        if(writing == 1) write_state <= W_FASE1;
        else             write_state <= WRITE_IDLE;
      W_FASE1:
      // Esta fase está demás de momento
        if(writing == 0) write_state <= WRITE_IDLE;
        else             write_state <= W_FASE2;
      W_FASE2:
      // Fase 2, se envia senal de pendig a NIOS, y se espera hasta que
      // el comando de writing baje a cero
        if(readwrite == 0) write_state <= W_FASE3;
        else               write_state <= W_FASE2;
      W_FASE3:
      // Fase 3, se lee el valor entrante y se ajusta la direccion en ram
        if(write_ready == 1) write_state <= WRITE_IDLE;
        else                 write_state <= W_FASE4;
      W_FASE4:
      // Fase 4, Se activa la escritura en ram
        if(write_ready == 1) write_state <= WRITE_IDLE;
        else                 write_state <= W_FASE5;
      W_FASE5:
      // Fase 5, se baja la escritura en ram y se lanza una senal de write_ready
      // interna para volver a la fase IDLE
        if(write_ready == 1) write_state <= WRITE_IDLE;
        else                 write_state <= W_FASE5;
    endcase
  end
end

// Acciones para writing
reg [BITDEPTH - 1 : 0] write_add; initial write_add = 0;
reg [BITDEPTH - 1 : 0] datawrite; initial datawrite = 0;
// reg write_ready; initial write_ready = 0;
reg write_pending; initial write_pending = 0;
reg write_enable; initial write_enable = 0;

always @(write_state) begin
  case(write_state)
    WRITE_IDLE: begin
      write_pending = 0;
      write_ready = 0;
      write_enable = 0;
    end
    W_FASE1: ;
    W_FASE2: write_pending = 1;
    W_FASE3: begin
      write_add = nios_address;
      datawrite = nios_value;
    end
    W_FASE4: write_enable = 1;
    W_FASE5: write_ready = 1;
  endcase
end

// Manejo senales de ready
assign ready = read_ready || write_ready;
wire [BYTEDEPTH - 1 : 0] write_value;

// Manejo de inout value
// Manejo de senal set_highz
wire set_highz;
assign set_highz = (writing)? 1 : 0;

assign nios_value = (set_highz)? HIGHZ : ram_q_a;
assign write_value = nios_value;



// Block RAM para control
// a side = NIOS II
// b side = FPGA

wire [BYTEDEPTH - 1 : 0] ram_q_a;

wire [BITDEPTH - 1 : 0] rw_address;
assign rw_address = (writing)? write_add: read_add;

true_dual_port_ram_dual_clock #(
  .DATA_WIDTH(BYTEDEPTH),
  .ADDR_WIDTH(ADDR_WIDTH)
  ) control_ram(
  .data_a (datawrite),
  .data_b (),
  .addr_a (rw_address),
  .addr_b (fpga_address),
  .we_a   (write_enable),
  .we_b   (GROUND),
  .clk_a  (clock_50),
  .clk_b  (clock_50),
  .q_a    (ram_q_a),
  .q_b    (fpga_value)
);

// Se sacan los datos de forma lenta de la FPGA
parameter HOWSLOW = 15; // a mayor numero mas lento
reg [HOWSLOW:0] accumulator; initial accumulator = 0;
reg slow_signal; initial slow_signal = 0;

always @(posedge clock_50 or negedge reset_n) begin
  if(!reset_n) accumulator <= 0;
  else
    if(accumulator == 0) begin
      slow_signal <= 1;
      accumulator <= accumulator + 1'b1;
    end
    else begin
      slow_signal <= 0;
      accumulator <= accumulator + 1'b1;
    end
end

// Maquina de estados para leer ram del lado de la FPGA (simple)

reg [1:0] read_fpga_state;
parameter READ_FPGA_IDLE = 2'd0, READ_FPGA_F1 = 2'd1, READ_FPGA_F2 = 2'd2,
          READ_FPGA_F3 = 2'd3;
initial read_fpga_state = READ_FPGA_IDLE;

always @(posedge clock_50 or negedge reset_n) begin
  if(!reset_n) read_fpga_state <= READ_FPGA_IDLE;
  else begin
    case(read_fpga_state)
      READ_FPGA_IDLE: begin
        if(slow_signal) read_fpga_state <= READ_FPGA_F1;
        else            read_fpga_state <= READ_FPGA_IDLE;
      end
      READ_FPGA_F1: begin
        if(slow_signal) read_fpga_state <= READ_FPGA_F2;
        else            read_fpga_state <= READ_FPGA_F1;
      end
      READ_FPGA_F2: begin
        if(slow_signal) read_fpga_state <= READ_FPGA_F3;
        else            read_fpga_state <= READ_FPGA_F2;
      end
      READ_FPGA_F3: begin
        if(slow_signal) read_fpga_state <= READ_FPGA_F1;
        else            read_fpga_state <= READ_FPGA_F3;
      end
    endcase
  end
end

reg  [BITDEPTH - 1 : 0]  fpga_address; initial fpga_address = 0;
wire [BYTEDEPTH - 1 : 0] fpga_value;
initial parameter_id    = 0;
initial parameter_value = 0;
initial new_data = 0;

always @(read_fpga_state) begin
  case(read_fpga_state)
    READ_FPGA_IDLE: fpga_address = 0;
    READ_FPGA_F1: begin
      // Bug, limites en cantidad de direcciones (limitar a usadas)
      new_data = 1;
      if(fpga_address < NUM_PARAM - 1)
        fpga_address = fpga_address + 1'b1;
      else
        fpga_address = 0;
    end
    READ_FPGA_F2: new_data = 0;
    READ_FPGA_F3: begin
      parameter_id = fpga_address;
      parameter_value = fpga_value;
      new_data = 0;
    end
  endcase
end

endmodule