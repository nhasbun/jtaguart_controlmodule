`include "debouncer/debouncer.v"
`include "control_module/control_module_pack.v"

module top
(
  // CLOCKS
  input CLOCK50, RESET_N,

  // SWITCH Y LEDS
  input  [3:0]SWITCH_ARRAY_IO,
  output [7:0]LED_ARRAY_IO
);

// Senal de reloj por un PLL por integridad
wire pll_50; wire pll_reset_n; wire pll_200;

pll pll_n1 (
  // ** input **
  .clock_50_clk         (CLOCK50),
  .reset_n_reset_n      (RESET_N),

  // ** output **
  .pll_50_locked_export (pll_reset_n),
  .pll_50_clk           (pll_50),
  .pll_200_clk          (pll_200)
);

wire [7:0] parameter_value;
wire [7:0] SWITCH_IO_NIOS;

// ** Debouncer para switch
wire [3:0] switch_array_debounced;

genvar ii;
generate
  for (ii = 0; ii < 4; ii = ii + 1) begin : DEBOUNCERS

    debouncer inst_debouncer(
      .clock50    (pll_50),
      .signal_in  (SWITCH_ARRAY_IO[ii]),
      .signal_out (switch_array_debounced[ii])
    );
  end
endgenerate

// ** Funcionamiento del modulo
assign LED_ARRAY_IO = parameter_value;
assign SWITCH_IO_NIOS = {{4{1'b0}}, {switch_array_debounced}}; //**
// senal debe ser de 8 bits

// ** Control Module (Pack +  NIOS)
control_module_pack control_module_pack1 (
    .clock_50        (pll_50),
    .reset_n         (pll_reset_n),
    .parameter_id    (),
    .parameter_value (parameter_value),
    .new_data        ()
  );

endmodule
