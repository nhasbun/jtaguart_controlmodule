
module niosii (
	address_external_connection_export,
	clock_50_clk,
	control_pio_pending_external_connection_export,
	control_pio_readwrite_external_connection_export,
	led_io_array_export,
	reset_n_reset_n,
	switch_io_array_export,
	value_external_connection_export);	

	output	[31:0]	address_external_connection_export;
	input		clock_50_clk;
	input	[7:0]	control_pio_pending_external_connection_export;
	output	[7:0]	control_pio_readwrite_external_connection_export;
	output	[7:0]	led_io_array_export;
	input		reset_n_reset_n;
	input	[7:0]	switch_io_array_export;
	inout	[31:0]	value_external_connection_export;
endmodule
