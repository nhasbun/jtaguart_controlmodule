// niosii_CONTROL_PIO.v

// Generated using ACDS version 16.1 203

`timescale 1 ps / 1 ps
module niosii_CONTROL_PIO (
		output wire [31:0] ADDRESS_external_connection_export,   //   ADDRESS_external_connection.export
		inout  wire [31:0] VALUE_external_connection_export,     //     VALUE_external_connection.export
		output wire        bridge_s0_waitrequest,                //                     bridge_s0.waitrequest
		output wire [31:0] bridge_s0_readdata,                   //                              .readdata
		output wire        bridge_s0_readdatavalid,              //                              .readdatavalid
		input  wire [0:0]  bridge_s0_burstcount,                 //                              .burstcount
		input  wire [31:0] bridge_s0_writedata,                  //                              .writedata
		input  wire [9:0]  bridge_s0_address,                    //                              .address
		input  wire        bridge_s0_write,                      //                              .write
		input  wire        bridge_s0_read,                       //                              .read
		input  wire [3:0]  bridge_s0_byteenable,                 //                              .byteenable
		input  wire        bridge_s0_debugaccess,                //                              .debugaccess
		input  wire        clock_50_clk,                         //                      clock_50.clk
		input  wire [7:0]  pending_external_connection_export,   //   pending_external_connection.export
		output wire [7:0]  readwrite_external_connection_export, // readwrite_external_connection.export
		input  wire        reset_n_reset_n                       //                       reset_n.reset_n
	);

	wire         bridge_m0_waitrequest;                     // mm_interconnect_0:BRIDGE_m0_waitrequest -> BRIDGE:m0_waitrequest
	wire  [31:0] bridge_m0_readdata;                        // mm_interconnect_0:BRIDGE_m0_readdata -> BRIDGE:m0_readdata
	wire         bridge_m0_debugaccess;                     // BRIDGE:m0_debugaccess -> mm_interconnect_0:BRIDGE_m0_debugaccess
	wire   [9:0] bridge_m0_address;                         // BRIDGE:m0_address -> mm_interconnect_0:BRIDGE_m0_address
	wire         bridge_m0_read;                            // BRIDGE:m0_read -> mm_interconnect_0:BRIDGE_m0_read
	wire   [3:0] bridge_m0_byteenable;                      // BRIDGE:m0_byteenable -> mm_interconnect_0:BRIDGE_m0_byteenable
	wire         bridge_m0_readdatavalid;                   // mm_interconnect_0:BRIDGE_m0_readdatavalid -> BRIDGE:m0_readdatavalid
	wire  [31:0] bridge_m0_writedata;                       // BRIDGE:m0_writedata -> mm_interconnect_0:BRIDGE_m0_writedata
	wire         bridge_m0_write;                           // BRIDGE:m0_write -> mm_interconnect_0:BRIDGE_m0_write
	wire   [0:0] bridge_m0_burstcount;                      // BRIDGE:m0_burstcount -> mm_interconnect_0:BRIDGE_m0_burstcount
	wire         mm_interconnect_0_address_s1_chipselect;   // mm_interconnect_0:ADDRESS_s1_chipselect -> ADDRESS:chipselect
	wire  [31:0] mm_interconnect_0_address_s1_readdata;     // ADDRESS:readdata -> mm_interconnect_0:ADDRESS_s1_readdata
	wire   [1:0] mm_interconnect_0_address_s1_address;      // mm_interconnect_0:ADDRESS_s1_address -> ADDRESS:address
	wire         mm_interconnect_0_address_s1_write;        // mm_interconnect_0:ADDRESS_s1_write -> ADDRESS:write_n
	wire  [31:0] mm_interconnect_0_address_s1_writedata;    // mm_interconnect_0:ADDRESS_s1_writedata -> ADDRESS:writedata
	wire         mm_interconnect_0_value_s1_chipselect;     // mm_interconnect_0:VALUE_s1_chipselect -> VALUE:chipselect
	wire  [31:0] mm_interconnect_0_value_s1_readdata;       // VALUE:readdata -> mm_interconnect_0:VALUE_s1_readdata
	wire   [1:0] mm_interconnect_0_value_s1_address;        // mm_interconnect_0:VALUE_s1_address -> VALUE:address
	wire         mm_interconnect_0_value_s1_write;          // mm_interconnect_0:VALUE_s1_write -> VALUE:write_n
	wire  [31:0] mm_interconnect_0_value_s1_writedata;      // mm_interconnect_0:VALUE_s1_writedata -> VALUE:writedata
	wire         mm_interconnect_0_readwrite_s1_chipselect; // mm_interconnect_0:READWRITE_s1_chipselect -> READWRITE:chipselect
	wire  [31:0] mm_interconnect_0_readwrite_s1_readdata;   // READWRITE:readdata -> mm_interconnect_0:READWRITE_s1_readdata
	wire   [1:0] mm_interconnect_0_readwrite_s1_address;    // mm_interconnect_0:READWRITE_s1_address -> READWRITE:address
	wire         mm_interconnect_0_readwrite_s1_write;      // mm_interconnect_0:READWRITE_s1_write -> READWRITE:write_n
	wire  [31:0] mm_interconnect_0_readwrite_s1_writedata;  // mm_interconnect_0:READWRITE_s1_writedata -> READWRITE:writedata
	wire  [31:0] mm_interconnect_0_pending_s1_readdata;     // PENDING:readdata -> mm_interconnect_0:PENDING_s1_readdata
	wire   [1:0] mm_interconnect_0_pending_s1_address;      // mm_interconnect_0:PENDING_s1_address -> PENDING:address
	wire         rst_controller_reset_out_reset;            // rst_controller:reset_out -> [ADDRESS:reset_n, BRIDGE:reset, PENDING:reset_n, READWRITE:reset_n, VALUE:reset_n, mm_interconnect_0:BRIDGE_reset_reset_bridge_in_reset_reset]

	niosii_CONTROL_PIO_ADDRESS address (
		.clk        (clock_50_clk),                            //                 clk.clk
		.reset_n    (~rst_controller_reset_out_reset),         //               reset.reset_n
		.address    (mm_interconnect_0_address_s1_address),    //                  s1.address
		.write_n    (~mm_interconnect_0_address_s1_write),     //                    .write_n
		.writedata  (mm_interconnect_0_address_s1_writedata),  //                    .writedata
		.chipselect (mm_interconnect_0_address_s1_chipselect), //                    .chipselect
		.readdata   (mm_interconnect_0_address_s1_readdata),   //                    .readdata
		.out_port   (ADDRESS_external_connection_export)       // external_connection.export
	);

	altera_avalon_mm_bridge #(
		.DATA_WIDTH        (32),
		.SYMBOL_WIDTH      (8),
		.HDL_ADDR_WIDTH    (10),
		.BURSTCOUNT_WIDTH  (1),
		.PIPELINE_COMMAND  (1),
		.PIPELINE_RESPONSE (1)
	) bridge (
		.clk              (clock_50_clk),                   //   clk.clk
		.reset            (rst_controller_reset_out_reset), // reset.reset
		.s0_waitrequest   (bridge_s0_waitrequest),          //    s0.waitrequest
		.s0_readdata      (bridge_s0_readdata),             //      .readdata
		.s0_readdatavalid (bridge_s0_readdatavalid),        //      .readdatavalid
		.s0_burstcount    (bridge_s0_burstcount),           //      .burstcount
		.s0_writedata     (bridge_s0_writedata),            //      .writedata
		.s0_address       (bridge_s0_address),              //      .address
		.s0_write         (bridge_s0_write),                //      .write
		.s0_read          (bridge_s0_read),                 //      .read
		.s0_byteenable    (bridge_s0_byteenable),           //      .byteenable
		.s0_debugaccess   (bridge_s0_debugaccess),          //      .debugaccess
		.m0_waitrequest   (bridge_m0_waitrequest),          //    m0.waitrequest
		.m0_readdata      (bridge_m0_readdata),             //      .readdata
		.m0_readdatavalid (bridge_m0_readdatavalid),        //      .readdatavalid
		.m0_burstcount    (bridge_m0_burstcount),           //      .burstcount
		.m0_writedata     (bridge_m0_writedata),            //      .writedata
		.m0_address       (bridge_m0_address),              //      .address
		.m0_write         (bridge_m0_write),                //      .write
		.m0_read          (bridge_m0_read),                 //      .read
		.m0_byteenable    (bridge_m0_byteenable),           //      .byteenable
		.m0_debugaccess   (bridge_m0_debugaccess),          //      .debugaccess
		.s0_response      (),                               // (terminated)
		.m0_response      (2'b00)                           // (terminated)
	);

	niosii_CONTROL_PIO_PENDING pending (
		.clk      (clock_50_clk),                          //                 clk.clk
		.reset_n  (~rst_controller_reset_out_reset),       //               reset.reset_n
		.address  (mm_interconnect_0_pending_s1_address),  //                  s1.address
		.readdata (mm_interconnect_0_pending_s1_readdata), //                    .readdata
		.in_port  (pending_external_connection_export)     // external_connection.export
	);

	niosii_LED_IO readwrite (
		.clk        (clock_50_clk),                              //                 clk.clk
		.reset_n    (~rst_controller_reset_out_reset),           //               reset.reset_n
		.address    (mm_interconnect_0_readwrite_s1_address),    //                  s1.address
		.write_n    (~mm_interconnect_0_readwrite_s1_write),     //                    .write_n
		.writedata  (mm_interconnect_0_readwrite_s1_writedata),  //                    .writedata
		.chipselect (mm_interconnect_0_readwrite_s1_chipselect), //                    .chipselect
		.readdata   (mm_interconnect_0_readwrite_s1_readdata),   //                    .readdata
		.out_port   (readwrite_external_connection_export)       // external_connection.export
	);

	niosii_CONTROL_PIO_VALUE value (
		.clk        (clock_50_clk),                          //                 clk.clk
		.reset_n    (~rst_controller_reset_out_reset),       //               reset.reset_n
		.address    (mm_interconnect_0_value_s1_address),    //                  s1.address
		.write_n    (~mm_interconnect_0_value_s1_write),     //                    .write_n
		.writedata  (mm_interconnect_0_value_s1_writedata),  //                    .writedata
		.chipselect (mm_interconnect_0_value_s1_chipselect), //                    .chipselect
		.readdata   (mm_interconnect_0_value_s1_readdata),   //                    .readdata
		.bidir_port (VALUE_external_connection_export)       // external_connection.export
	);

	niosii_CONTROL_PIO_mm_interconnect_0 mm_interconnect_0 (
		.CLOCK_clk_clk                            (clock_50_clk),                              //                          CLOCK_clk.clk
		.BRIDGE_reset_reset_bridge_in_reset_reset (rst_controller_reset_out_reset),            // BRIDGE_reset_reset_bridge_in_reset.reset
		.BRIDGE_m0_address                        (bridge_m0_address),                         //                          BRIDGE_m0.address
		.BRIDGE_m0_waitrequest                    (bridge_m0_waitrequest),                     //                                   .waitrequest
		.BRIDGE_m0_burstcount                     (bridge_m0_burstcount),                      //                                   .burstcount
		.BRIDGE_m0_byteenable                     (bridge_m0_byteenable),                      //                                   .byteenable
		.BRIDGE_m0_read                           (bridge_m0_read),                            //                                   .read
		.BRIDGE_m0_readdata                       (bridge_m0_readdata),                        //                                   .readdata
		.BRIDGE_m0_readdatavalid                  (bridge_m0_readdatavalid),                   //                                   .readdatavalid
		.BRIDGE_m0_write                          (bridge_m0_write),                           //                                   .write
		.BRIDGE_m0_writedata                      (bridge_m0_writedata),                       //                                   .writedata
		.BRIDGE_m0_debugaccess                    (bridge_m0_debugaccess),                     //                                   .debugaccess
		.ADDRESS_s1_address                       (mm_interconnect_0_address_s1_address),      //                         ADDRESS_s1.address
		.ADDRESS_s1_write                         (mm_interconnect_0_address_s1_write),        //                                   .write
		.ADDRESS_s1_readdata                      (mm_interconnect_0_address_s1_readdata),     //                                   .readdata
		.ADDRESS_s1_writedata                     (mm_interconnect_0_address_s1_writedata),    //                                   .writedata
		.ADDRESS_s1_chipselect                    (mm_interconnect_0_address_s1_chipselect),   //                                   .chipselect
		.PENDING_s1_address                       (mm_interconnect_0_pending_s1_address),      //                         PENDING_s1.address
		.PENDING_s1_readdata                      (mm_interconnect_0_pending_s1_readdata),     //                                   .readdata
		.READWRITE_s1_address                     (mm_interconnect_0_readwrite_s1_address),    //                       READWRITE_s1.address
		.READWRITE_s1_write                       (mm_interconnect_0_readwrite_s1_write),      //                                   .write
		.READWRITE_s1_readdata                    (mm_interconnect_0_readwrite_s1_readdata),   //                                   .readdata
		.READWRITE_s1_writedata                   (mm_interconnect_0_readwrite_s1_writedata),  //                                   .writedata
		.READWRITE_s1_chipselect                  (mm_interconnect_0_readwrite_s1_chipselect), //                                   .chipselect
		.VALUE_s1_address                         (mm_interconnect_0_value_s1_address),        //                           VALUE_s1.address
		.VALUE_s1_write                           (mm_interconnect_0_value_s1_write),          //                                   .write
		.VALUE_s1_readdata                        (mm_interconnect_0_value_s1_readdata),       //                                   .readdata
		.VALUE_s1_writedata                       (mm_interconnect_0_value_s1_writedata),      //                                   .writedata
		.VALUE_s1_chipselect                      (mm_interconnect_0_value_s1_chipselect)      //                                   .chipselect
	);

	altera_reset_controller #(
		.NUM_RESET_INPUTS          (1),
		.OUTPUT_RESET_SYNC_EDGES   ("deassert"),
		.SYNC_DEPTH                (2),
		.RESET_REQUEST_PRESENT     (0),
		.RESET_REQ_WAIT_TIME       (1),
		.MIN_RST_ASSERTION_TIME    (3),
		.RESET_REQ_EARLY_DSRT_TIME (1),
		.USE_RESET_REQUEST_IN0     (0),
		.USE_RESET_REQUEST_IN1     (0),
		.USE_RESET_REQUEST_IN2     (0),
		.USE_RESET_REQUEST_IN3     (0),
		.USE_RESET_REQUEST_IN4     (0),
		.USE_RESET_REQUEST_IN5     (0),
		.USE_RESET_REQUEST_IN6     (0),
		.USE_RESET_REQUEST_IN7     (0),
		.USE_RESET_REQUEST_IN8     (0),
		.USE_RESET_REQUEST_IN9     (0),
		.USE_RESET_REQUEST_IN10    (0),
		.USE_RESET_REQUEST_IN11    (0),
		.USE_RESET_REQUEST_IN12    (0),
		.USE_RESET_REQUEST_IN13    (0),
		.USE_RESET_REQUEST_IN14    (0),
		.USE_RESET_REQUEST_IN15    (0),
		.ADAPT_RESET_REQUEST       (0)
	) rst_controller (
		.reset_in0      (~reset_n_reset_n),               // reset_in0.reset
		.clk            (clock_50_clk),                   //       clk.clk
		.reset_out      (rst_controller_reset_out_reset), // reset_out.reset
		.reset_req      (),                               // (terminated)
		.reset_req_in0  (1'b0),                           // (terminated)
		.reset_in1      (1'b0),                           // (terminated)
		.reset_req_in1  (1'b0),                           // (terminated)
		.reset_in2      (1'b0),                           // (terminated)
		.reset_req_in2  (1'b0),                           // (terminated)
		.reset_in3      (1'b0),                           // (terminated)
		.reset_req_in3  (1'b0),                           // (terminated)
		.reset_in4      (1'b0),                           // (terminated)
		.reset_req_in4  (1'b0),                           // (terminated)
		.reset_in5      (1'b0),                           // (terminated)
		.reset_req_in5  (1'b0),                           // (terminated)
		.reset_in6      (1'b0),                           // (terminated)
		.reset_req_in6  (1'b0),                           // (terminated)
		.reset_in7      (1'b0),                           // (terminated)
		.reset_req_in7  (1'b0),                           // (terminated)
		.reset_in8      (1'b0),                           // (terminated)
		.reset_req_in8  (1'b0),                           // (terminated)
		.reset_in9      (1'b0),                           // (terminated)
		.reset_req_in9  (1'b0),                           // (terminated)
		.reset_in10     (1'b0),                           // (terminated)
		.reset_req_in10 (1'b0),                           // (terminated)
		.reset_in11     (1'b0),                           // (terminated)
		.reset_req_in11 (1'b0),                           // (terminated)
		.reset_in12     (1'b0),                           // (terminated)
		.reset_req_in12 (1'b0),                           // (terminated)
		.reset_in13     (1'b0),                           // (terminated)
		.reset_req_in13 (1'b0),                           // (terminated)
		.reset_in14     (1'b0),                           // (terminated)
		.reset_req_in14 (1'b0),                           // (terminated)
		.reset_in15     (1'b0),                           // (terminated)
		.reset_req_in15 (1'b0)                            // (terminated)
	);

endmodule
