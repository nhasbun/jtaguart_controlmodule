	niosii u0 (
		.address_external_connection_export               (<connected-to-address_external_connection_export>),               //               address_external_connection.export
		.clock_50_clk                                     (<connected-to-clock_50_clk>),                                     //                                  clock_50.clk
		.control_pio_pending_external_connection_export   (<connected-to-control_pio_pending_external_connection_export>),   //   control_pio_pending_external_connection.export
		.control_pio_readwrite_external_connection_export (<connected-to-control_pio_readwrite_external_connection_export>), // control_pio_readwrite_external_connection.export
		.led_io_array_export                              (<connected-to-led_io_array_export>),                              //                              led_io_array.export
		.reset_n_reset_n                                  (<connected-to-reset_n_reset_n>),                                  //                                   reset_n.reset_n
		.switch_io_array_export                           (<connected-to-switch_io_array_export>),                           //                           switch_io_array.export
		.value_external_connection_export                 (<connected-to-value_external_connection_export>)                  //                 value_external_connection.export
	);

