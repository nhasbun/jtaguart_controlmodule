// UTF-8 Encoding

#include "main.h"

#ifdef LED_IO_BASE
#elif SWITCH_IO_BASE
#elif CONTROL_PIO_ADDRESS_BASE
#elif CONTROL_PIO_VALUE_BASE
#elif CONTROL_PIO_READWRITE_BASE
#elif CONTROL_PIO_PENDING_BASE
#endif

#define control_module_address         CONTROL_PIO_ADDRESS_BASE
#define control_module_value           CONTROL_PIO_VALUE_BASE
#define control_module_value_direction (CONTROL_PIO_VALUE_BASE + 4)
#define control_module_rw              CONTROL_PIO_READWRITE_BASE
#define control_module_pending         CONTROL_PIO_PENDING_BASE

#define DELAY 100
#define BUF 32

#define MENSAJE "*** INICIANDO SISTEMA ***\n"
#define ESPERANDO "esperando comando...\n"
#define COM_NO_RECONOCIDO "comando no reconocido...\n"
#define SAVE_EJECUTADO "SAVE EJECUTADO\n"
#define READ_EJECUTADO "READ EJECUTADO\n"
#define PENDING_MSG "Pending detectado...\n"
#define READ_MSG "Leyendo value entregado...\n"

int main()
{
  // ** Inicio rutina

  alt_putstr(MENSAJE);

  char comando[BUF] = { 0 }; // **
  // Idealmente aca iría un malloc(), pero esto permite ahorrar esa librería
  // sin mayores problemas.

  while(1) {
    alt_putstr(ESPERANDO);

    leer_uart(comando);
    alt_putstr(comando); alt_putstr("\n");

    if (*comando == 's') {
      rutina_escritura();
      // leer_uart(address);
      // alt_putstr(address);
      // alt_putstr(SAVE_EJECUTADO);
    }

    else if (*comando == 'r') {
      rutina_lectura();
    }

    else
      alt_putstr(COM_NO_RECONOCIDO);
    usleep(DELAY);
  }
  return 0;
}

void leer_uart(char * comando)
{
  while(1) {
    *comando = alt_getchar();
    if (*comando == '\n') break;
    comando++;
  }

  *comando = '\0'; // cierre obligatorio de string y sobreescribimos '\n'
}

void rutina_lectura()
{
  char address[BUF] = { 0 }; // **
  //char value[BUF]   = { 0 }; // **

  leer_uart(address);
  alt_putstr(address); alt_putstr("\n");
  uint32_t int_add = my_atoi(address);

  alt_write_word(control_module_address, int_add);
  alt_write_word(control_module_value_direction, 0); // 0 input, 1 output
  alt_write_word(control_module_rw, 1);

  while(1) {
    if(alt_read_word(control_module_pending) == 1) break;
  }

  alt_write_word(control_module_rw, 0);
  alt_putstr(PENDING_MSG);
  uint32_t value = 0;

  while(1) {
    if(alt_read_word(control_module_pending) == 0) break;
  }

  alt_putstr(READ_MSG);
  value = alt_read_word(control_module_value);

  alt_printf("0x%x\n", value);
  alt_putstr(READ_EJECUTADO);
}

void rutina_escritura()
{
  char address[BUF] = { 0 }; // **
  char value[BUF]   = { 0 }; // **

  leer_uart(address); alt_putstr(address); alt_putstr("\n");
  leer_uart(value); alt_putstr(value); alt_putstr("\n");

  uint32_t int_add = my_atoi(address);
  uint32_t int_val = my_atoi(value);

  alt_write_word(control_module_address, int_add);
  alt_write_word(control_module_value_direction, 0); // 0 input, 1 output

  alt_write_word(control_module_rw, 2);

  // Esperar señal de pending
  while(1) {
    if(alt_read_word(control_module_pending) == 1) break;
  }

  // resgistro value como output
  alt_write_word(control_module_value_direction, 1); // 0 input, 1 output
  alt_write_word(control_module_value, int_val); // 0 input, 1 output
  alt_write_word(control_module_rw, 0); /* se baja señal de write cuando se
  detecta pending */

  // Se espera que termine el proceso de escritura interno
  while(1) {
    if(alt_read_word(control_module_pending) == 0) break;
  }
  alt_putstr(SAVE_EJECUTADO);
}
